#include "ITG3200.h"

itg3200_t itg3200_offset;

void initITG3200(itg3200_dlpf_cfg_t dlpf_cfg)
{
	I2C_MESSAGE_STATUS status = I2C_MESSAGE_PENDING;
	{
		uint8_t writeBuffer[] = {ITG3200_SMPLRT_DIV_REG, 0x00};
		I2C_MasterWrite(writeBuffer, sizeof (writeBuffer), ITG3200_I2C_ADDR, &status);
		while (status == I2C_MESSAGE_PENDING);
	}
	{
		uint8_t writeBuffer[] = {ITG3200_DLPF_FS_REG, ITG3200_FS_SEL};
		writeBuffer[1] |= dlpf_cfg;
		I2C_MasterWrite(writeBuffer, sizeof (writeBuffer), ITG3200_I2C_ADDR, &status);
		while (status == I2C_MESSAGE_PENDING);
	}
	calibrateITG3200();
}

void configITG3200(itg3200_config_t config)
{
	I2C_MESSAGE_STATUS status = I2C_MESSAGE_PENDING;
	{
		uint8_t writeBuffer[] = {ITG3200_PWR_MGM_REG, ITG3200_STANDBY_MODE};
		I2C_MasterWrite(writeBuffer, sizeof (writeBuffer), ITG3200_I2C_ADDR, &status);
		while (status == I2C_MESSAGE_PENDING);
	}
	{
		uint8_t writeBuffer[] = {ITG3200_SMPLRT_DIV_REG, 0x00};
		writeBuffer[1] = config.sample_rate_divider;
		I2C_MasterWrite(writeBuffer, sizeof (writeBuffer), ITG3200_I2C_ADDR, &status);
		while (status == I2C_MESSAGE_PENDING);
	}
	{
		uint8_t writeBuffer[] = {ITG3200_DLPF_FS_REG, ITG3200_FS_SEL};
		writeBuffer[1] |= config.dlpf_cfg;
		I2C_MasterWrite(writeBuffer, sizeof (writeBuffer), ITG3200_I2C_ADDR, &status);
		while (status == I2C_MESSAGE_PENDING);
	}
}

void startITG3200()
{
	I2C_MESSAGE_STATUS status = I2C_MESSAGE_PENDING;
	uint8_t writeBuffer[] = {ITG3200_PWR_MGM_REG, ITG3200_MEASUREMENT_MODE};
	I2C_MasterWrite(writeBuffer, sizeof (writeBuffer), ITG3200_I2C_ADDR, &status);
	while (status == I2C_MESSAGE_PENDING);
}

void stopITG3200()
{
	I2C_MESSAGE_STATUS status = I2C_MESSAGE_PENDING;
	uint8_t writeBuffer[] = {ITG3200_PWR_MGM_REG, ITG3200_STANDBY_MODE};
	I2C_MasterWrite(writeBuffer, sizeof (writeBuffer), ITG3200_I2C_ADDR, &status);
	while (status == I2C_MESSAGE_PENDING);
}

void calibrateITG3200()
{
	const uint8_t nr_measurements = 10;
	int16_t x = 0;
	int16_t y = 0;
	int16_t z = 0;
	I2C_MESSAGE_STATUS status = I2C_MESSAGE_PENDING;
	for (uint8_t i = 0; i < nr_measurements; i++)
	{
		__delay_ms(100);
		uint8_t writeBuffer[] = {ITG3200_DATA_REG};
		I2C_MasterWrite(writeBuffer, sizeof (writeBuffer), ITG3200_I2C_ADDR, &status);
		while (status == I2C_MESSAGE_PENDING);
		uint8_t readBuffer[8];
		I2C_MasterRead(readBuffer, sizeof (readBuffer), ITG3200_I2C_ADDR, &status);
		while (status == I2C_MESSAGE_PENDING);
		x += ((readBuffer[4] << 8) | readBuffer[5]);
		y += ((readBuffer[2] << 8) | readBuffer[3]);
		z += ((readBuffer[6] << 8) | readBuffer[7]);
	}
	itg3200_offset.x = x / nr_measurements;
	itg3200_offset.y = y / nr_measurements;
	itg3200_offset.z = z / nr_measurements;
}

void readITG3200(itg3200_t *data)
{
	I2C_MESSAGE_STATUS status = I2C_MESSAGE_PENDING;
	uint8_t writeBuffer[] = {ITG3200_DATA_REG};
	I2C_MasterWrite(writeBuffer, sizeof (writeBuffer), ITG3200_I2C_ADDR, &status);
	while (status == I2C_MESSAGE_PENDING);
	uint8_t readBuffer[8];
	I2C_MasterRead(readBuffer, sizeof (readBuffer), ITG3200_I2C_ADDR, &status);
	while (status == I2C_MESSAGE_PENDING);
	data->x = (((readBuffer[4] << 8) | readBuffer[5]) - itg3200_offset.x) / 14.375;
	data->y = (((readBuffer[2] << 8) | readBuffer[3]) - itg3200_offset.y) / 14.375;
	data->z = (((readBuffer[6] << 8) | readBuffer[7]) - itg3200_offset.z) / 14.375;
	data->temp = (readBuffer[0] << 8) | readBuffer[1];
	data->temp = ((data->temp + 13200) / 280) + 35;
}
