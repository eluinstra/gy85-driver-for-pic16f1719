/*
 * File:   main.c
 * Author: eluinstra
 *
 * Created on January 30, 2016, 4:00 PM
 */


#include "mcc_generated_files/mcc.h"
#include "GY85.h"

gy85_config_t gy85_config;
gy85_t gy85;

void main(void)
{
	// initialize the device
	SYSTEM_Initialize();

	// Enable the Global Interrupts
	INTERRUPT_GlobalInterruptEnable();

	// Enable the Peripheral Interrupts
	INTERRUPT_PeripheralInterruptEnable();

	/*Clear screen and set cursor to the start of the current line*/
	printf("\033[2J\033[0;0H");
	printf("Microchip GY-85 Demo\r\n");

	gy85_config.accelerometer.low_power_mode = false;
	gy85_config.accelerometer.output_rate = ADXL345_100Hz;
	gy85_config.accelerometer.full_res_mode = false;
	gy85_config.accelerometer.left_justified_mode = false;
	gy85_config.accelerometer.range = ADXL345_4G;
	
	gy85_config.compass.samples_per_output = HMC5883_1_SAMPLE;
	gy85_config.compass.output_rate = HMC5883_15Hz;
	gy85_config.compass.gain = HMC5883_1090_Gain;
	
	gy85_config.gyroscope.sample_rate_divider = 0x05;
	gy85_config.gyroscope.dlpf_cfg = ITG3200_5Hz;
	
	configGY85(gy85_config);
	startGY85();
	__delay_ms(1000);
	calibrateGY85();
	while (1)
	{
		readGY85(&gy85);
		printf("accelerometer x: %d, y: %d, z: %d\r\n", gy85.accelerometer.x, gy85.accelerometer.y, gy85.accelerometer.z);
		printf("compass x: %d, y: %d, z: %d\r\n", gy85.compass.x, gy85.compass.y, gy85.compass.z);
		printf("gyro x: %f, y: %f, z: %f\r\n", gy85.gyroscope.x, gy85.gyroscope.y, gy85.gyroscope.z);
		printf("temperature: %f\r\n", gy85.gyroscope.temp);
		__delay_ms(1000);
	}
}
