#include "GY85.h"
#include "HMC5883.h"
#include "ITG3200.h"
#include "ADXL345.h"

void initGY85(accelerometer_range_t range, gyroscope_bandwidth_t bandwidth)
{
	initADXL345(range);
	initHMC5883();
	initITG3200(bandwidth);
}

void configGY85(gy85_config_t config)
{
	configADXL345(config.accelerometer);
	configHMC5883(config.compass);
	configITG3200(config.gyroscope);
}

void startGY85()
{
	startADXL345();
	startHMC5883();
	startITG3200();
}

void stopGY85()
{
	stopADXL345();
	stopHMC5883();
	stopITG3200();
}

void calibrateGY85()
{
	calibrateITG3200();
}

void readGY85(gy85_t *data)
{
	readADXL345(&data->accelerometer);
	readHMC5883(&data->compass);
	readITG3200(&data->gyroscope);
}